import Data.Char
import Data.List

main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> Integer -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ show res

solveAll :: [String] -> [Integer]
solveAll [] = []
solveAll (a:resto) = solveOne (reverse a) : solveAll resto

solveOne:: String -> Integer
solveOne [] = 0
solveOne (b:xs) = foldl (\x y -> y + x*10) 0 (solveT [b'] b' xs)
  where
    b' = toInteger $ digitToInt b

solveT :: [Integer] -> Integer -> String -> [Integer]
solveT xs _ "" = xs
solveT ys n (x:xs) = solveT (r2:y1) r2 xs
  where
    t1 = toInteger $ digitToInt x
    r1 = if n < t1 || n == 0 then 9 else n
    r2 = if n < t1 then mod (t1-1) 10 else t1
    y1 = if n < t1 || n == 0 then replicate (length ys) 9 else ys
